from django.shortcuts import render
from perfis.models import Usuario


def index(request):
    return render(request, 'listadoUsuarios.html', {'usuarios' : Usuario.objects.all()})


def exibir(request, id_usuario):
    
    usuario = Usuario.objects.get(id= id_usuario)

    return render(request, 'perfil.html', {"usuario" : usuario})

def cadastro(request):

    return render(request, 'cadastroUsuario.html')