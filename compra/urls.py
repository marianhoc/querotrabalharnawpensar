from django.urls import path, re_path
from compra import views
from django.conf.urls import url
from compra.views import RegistrarNovaCompra


urlpatterns = [
    path('', views.index, name="listaCompras"),
    path('<int:id_compra>/detalhes', views.exibir, name="detalheCompra"),
    path('nova', RegistrarNovaCompra.as_view(), name="novaCompra"),
    
    
    #path('<int:id_compra>', views.exibir, name="exibirProduto"),
    #path('cadastrar/', RegistrarProdutoView.as_view(), name="cadastroProduto")
]

