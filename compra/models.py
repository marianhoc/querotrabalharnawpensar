from django.db import models
from datetime import date
from produto.models import Produto

class Compra(models.Model):

    date = models.DateField(("Date"), default=date.today)
    #produtos = models.ManyToManyField(Produto, related_name="compras")
    total = models.DecimalField(max_digits=5, decimal_places=2, default=0.0)


    @property
    def unidadesEmUmaCompra(self):
        total=0
        #for produto in self.historico.produtos.all():
        for produto in HistoricoCompras.objects.get(compra= self).produtos.all():
            
            total = total + produto.quantidade
                
        return total


class HistoricoCompras(models.Model):

    compra = models.ForeignKey(Compra, related_name="historico", on_delete=models.CASCADE)
    produtos = models.ManyToManyField(Produto, related_name="compras")