# Generated by Django 2.0.6 on 2018-06-25 00:14

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('produto', '0004_auto_20180624_2342'),
        ('compra', '0006_auto_20180624_2349'),
    ]

    operations = [
        migrations.CreateModel(
            name='HistoricoCompras',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
        ),
        migrations.RemoveField(
            model_name='compra',
            name='produtos',
        ),
        migrations.AddField(
            model_name='historicocompras',
            name='compra',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='historico', to='compra.Compra'),
        ),
        migrations.AddField(
            model_name='historicocompras',
            name='produtos',
            field=models.ManyToManyField(related_name='compras', to='produto.Produto'),
        ),
    ]
