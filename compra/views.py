from django.shortcuts import render
from compra.models import Compra, HistoricoCompras
from produto.models import Produto
from django.shortcuts import redirect
from django.views.generic.base import View
#from produto.forms import RegistrarProdutoForm


def index(request):

    return render(request, 'listaCompras.html' , {'compras' : Compra.objects.all()})


def exibir(request, id_compra):
    # pego o objeto COMPRA do BD
    compra = Compra.objects.get(id=id_compra)
    historico = HistoricoCompras.objects.get(compra=compra)

    # e separo em outra variavel a lista de produtos comprados
    produtos = historico.produtos.all()

    # crio uma lista de 1 ate o numero de produtos na compra para
    # mostrar na tabela
    itens = list(range(1, len(produtos)+1))


    # a nova lista de compras contendo os indices e os objetos produto vai montar a tabela de detalhe
    listaDeCompras = zip(itens, produtos)

    return render(request, 'detalheCompra.html', 
                                {"produtos" : listaDeCompras ,
                                "compra" : compra })




class RegistrarNovaCompra(View):

    def get(self, request):

        return render(request, 'novaCompra.html', {'produtos' : Produto.objects.all()})


    def post(self, request):
        produtosComprados = []

        for item in request.POST:
            
            if (item != "csrfmiddlewaretoken" and request.POST[item] > '0' ):

                #pego o Produto e atualizo a quantidade que vai ficar no estoque  
                produto = Produto.objects.get(nome=item)
                produto.quantidade = produto.quantidade + int(request.POST[item])
                produto.save()
                
                # quantidade do produto recebe o valor indicado no form de compra
                produto.quantidade = int(request.POST[item])
                
                # e crio uma lista dos produtos escolhidos
                produtosComprados.append(produto)
                             

        # se tem produtos escolhidos vou criar a nova compra e cadastrar no historico de produtos
        if (len(produtosComprados)>0):
            compra = Compra.objects.create(total=0)
            historico = HistoricoCompras.objects.create(compra = compra)

            for produto in produtosComprados:
                #compra.produtos.add(produto)

                print("= = = = = = view compra 71 = = = = = =")
                print(produto.quantidade)
                historico.produtos.add(produto)                
                compra.total = compra.total + produto.valor 
          
            compra.save()  
            historico.save()  
        return redirect('listaCompras')
