from django.urls import path, re_path
from produto import views
from django.conf.urls import url
from produto.views import RegistrarProdutoView


urlpatterns = [
    path('', views.index, name="index"),
    #path('cadastro/', views.cadastro),
    path('<int:id_produto>', views.exibir, name="exibirProduto"),
    path('<int:id_produto>/edit', views.edit, name="editarProduto"),
    path('cadastrar/', RegistrarProdutoView.as_view(), name="cadastroProduto")
]

