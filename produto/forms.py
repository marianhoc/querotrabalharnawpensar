from django import forms
from produto.models import Produto

class RegistrarProdutoForm(forms.Form):

    nome = forms.CharField(required=True)
    descricao = forms.CharField(required=True)
    valor = forms.CharField(required=True)
    quantidade = forms.CharField(required=True)

    

    def is_valid(self):

        valid = True
        if not super(RegistrarProdutoForm, self).is_valid():
            self.adicionar_erro('dados incorretos.Tente novamente')
            valid = False
        
        produtoJaExiste = Produto.objects.filter(nome=self.data['nome']).exists()

        if produtoJaExiste:
            self.adicionar_erro('Produto: "{}" ja existe'.format(self.data['nome']))
            valid = False

        return valid


    def adicionar_erro(self, message):
        erros = self._errors.setdefault(forms.forms.NON_FIELD_ERRORS, forms.utils.ErrorList())
        erros.append(message)


    