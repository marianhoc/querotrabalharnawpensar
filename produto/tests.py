from django.test import TestCase
from produto.models import Produto
from django.urls import reverse, resolve

class ProdutoTestCase(TestCase):
 
    def test_exibir(self):
        path = reverse("exibirProduto", kwargs={'id_produto' : 1})
        self.assertEqual(resolve(path).view_name , "exibirProduto") 
    
    def test_tem_estoque(self):
        produto = Produto.objects.create(nome = "test", valor=9.99, descricao='', quantidade=1)
        self.assertEqual(produto.tem_estoque, True)

        produto.quantidade = 0
        self.assertEqual(produto.tem_estoque, False)

    def test_cadastro_produto(self):
        
        pass
