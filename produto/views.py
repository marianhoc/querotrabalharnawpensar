from django.shortcuts import render
from produto.models import Produto
from django.shortcuts import redirect
from django.views.generic.base import View
from produto.forms import RegistrarProdutoForm

def index(request):
    
    return render(request, 'index.html', {'produtos' : Produto.objects.all()})

def edit(request, id_produto):
    return redirect('index')


def exibir(request, id_produto):
    
    produto = Produto.objects.get(id= id_produto)

    return render(request, 'produto.html', {"produto" : produto})





class RegistrarProdutoView(View):

    def get(self, request):
        return render(request, 'cadastro.html')


    def post(self, request):
        form = RegistrarProdutoForm(request.POST)

        if form.is_valid():
            
            produto = Produto.objects.create(nome=form.data['nome'], 
                                             descricao=form.data['descricao'],
                                             valor=form.data['valor'], 
                                             quantidade=form.data['quantidade'])    
            produto.save()

            return redirect('index')

        return render(request, 'cadastro.html', {'form': form})
