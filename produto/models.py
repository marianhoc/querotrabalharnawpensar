from django.db import models


class Produto(models.Model):

        nome = models.CharField(max_length=225, null=False)
        descricao = models.CharField(max_length=225, null=False)
        valor = models.DecimalField(max_digits=5, decimal_places=2)
        quantidade = models.IntegerField(default = 0)
        

        # returna TRUE quando a quantidade é maior a 0
        # retorna FALSE em todos os outros casos 
        @property
        def tem_estoque(self):
                return self.quantidade > 0